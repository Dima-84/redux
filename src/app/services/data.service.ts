import { EditTask } from './../redux/tasks/tasks.action';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Task } from '../models/task';
import { Store } from '@ngrx/store';
import { LoadTasks, DeleteTask } from '../redux/tasks/tasks.action';
import { TasksState } from '../redux/tasks/tasks.state';

@Injectable({ providedIn: 'root' })
export class DataService {
  private readonly tasks: Task[] = [
    {
      id: 0,
      text: 'Go to shop',
      date: moment()
    },
    {
      id: 1,
      text: 'Sleep',
      date: moment()
    },
    {
      id: 2,
      text: 'Buy a car',
      date: moment()
    }
  ];

  constructor(private taskStore: Store<TasksState>) { }

  loadTask(): void {
      setTimeout(() => {
        this.taskStore.dispatch(new LoadTasks(this.tasks));
      }, 2000);
    }

    deleteTask(id: number) {
      const task = this.tasks.find(t => t.id === id);
      const index = this.tasks.indexOf(task);
      this.tasks.splice(index, 1);
      this.taskStore.dispatch(new DeleteTask(id));
    }

    editTask(task: Task): void {
      const editableTask = this.tasks.find(t => t.id === task.id);
      const index = this.tasks.indexOf(editableTask);
      this.tasks.splice(index, 1);
      this.tasks.push(task);
      this.taskStore.dispatch(new EditTask(task));
    }

  }
