import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EditComponent } from './edit.component';
import { TaskItemModule } from '../task-item/task-item.module';


@NgModule({
  declarations: [
    EditComponent
  ],
  imports: [
    CommonModule,
    TaskItemModule,
    RouterModule.forChild([
      {
        path: '',
        component: EditComponent
      }
    ])
  ]
})

export class EditModule { }
