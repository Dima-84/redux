import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { TasksState } from '../redux/tasks/tasks.state';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent {

  constructor(private tasksStore: Store<TasksState>) { }

  public get tasksData() {
    return this.tasksStore.select('tasksData');
  }
}
