import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ListComponent } from './list.component';
import { TaskItemModule } from '../task-item/task-item.module';


@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    TaskItemModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListComponent
      }
    ])
  ]
})

export class ListModule { }
