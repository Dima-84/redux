import { Component } from '@angular/core';
import { TasksState } from '../redux/tasks/tasks.state';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent  {

  constructor(private tasksStore: Store<TasksState>) { }

    public get taskData() {
      return this.tasksStore.select('tasksData');
    }
}
