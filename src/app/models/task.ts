import * as moment from 'moment';

export interface Task {
  id: number;
  text: string;
  date: moment.Moment;
}
