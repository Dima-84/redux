import { TasksState } from './redux/tasks/tasks.state';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoadTasks } from './redux/tasks/tasks.action';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Redux';

  constructor(private dataservice: DataService) { }

  ngOnInit() {
    this.dataservice.loadTask();
  }

  }
