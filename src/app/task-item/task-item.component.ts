import { Component, Input } from '@angular/core';
import { Task } from '../models/task';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})

export class TaskItemComponent {

  public static taskEdittableId: number;

  @Input() task: Task;
  @Input() isEditable: boolean;

  // tslint:disable-next-line: variable-name
  private _isEdit: boolean;

  constructor(private dataService: DataService) { }

  public deleteItem(): void {
    this.dataService.deleteTask(this.task.id);
  }

  public get isEdit(): boolean {
    return this._isEdit && TaskItemComponent.taskEdittableId === this.task.id;
  }

  public enableEdit() {
    this._isEdit = true;
    TaskItemComponent.taskEdittableId = this.task.id;
  }

  public disableEdit(): void {
    this._isEdit = false;
  }

  private editItem(): void {
    this.dataService.editTask(this.task);
  }

  public saveChanges() {
    this.editItem();
    this.disableEdit();
  }
}
