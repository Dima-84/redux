import { TaskItemComponent } from './task-item.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    TaskItemComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    TaskItemComponent
  ]
})

export class TaskItemModule {

  constructor() { }
}
